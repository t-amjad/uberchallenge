package com.ta.uberchallenge.utils

import android.widget.ImageView
import com.ta.uberchallenge.network.image.ImageLoader

class FakeImageLoader : ImageLoader {
    override fun displayImage(placeholder: Int, url: String, imageView: ImageView) {}
}