package com.ta.uberchallenge.home.presenter

import android.content.Context
import com.ta.uberchallenge.home.model.FlickrPhoto
import com.ta.uberchallenge.home.model.Response
import com.ta.uberchallenge.utils.whenever
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class FlickrPresenterTest {

    private lateinit var tested: FlickrPresenter
    @Mock lateinit var view: FlickrPresenter.View
    @Mock lateinit var context: Context

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        tested = FlickrPresenter(context, view)
    }

    @Test
    fun onTaskCompleted_should_call_setEmptyView() {
        whenever(context.getString(anyInt())).thenReturn("string")
        tested.onTaskCompleted(Response(0, emptyList()))

        verify(view, times(1)).hideProgress()
        verify(view, times(1)).setEmptyView(true, "string")
    }

    @Test
    fun onTaskCompleted_should_call_showData() {
        val list = listOf(
            FlickrPhoto("id", "secret", "server", 0, "title")
                         )
        tested.onTaskCompleted(Response(0, list))

        verify(view, times(1)).hideProgress()
        verify(view, times(1)).setEmptyView(false)
        verify(view, times(1)).showData(list)
    }

    @Test
    fun onTaskCompleted_should_call_showError() {
        val response = Response(0, emptyList(), "error")
        tested.onTaskCompleted(response)

        verify(view, times(1)).hideProgress()
        verify(view, times(1)).showError(anyString())
    }
}