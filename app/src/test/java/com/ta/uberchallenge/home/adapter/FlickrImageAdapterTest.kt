package com.ta.uberchallenge.home.adapter

import com.ta.uberchallenge.home.model.FlickrPhoto
import com.ta.uberchallenge.network.image.ImageLoader
import com.ta.uberchallenge.utils.FakeImageLoader
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class FlickrImageAdapterTest {

    private lateinit var test: FlickrImageAdapter
    private lateinit var imageLoader: ImageLoader

    @Before
    fun setUp() {
        imageLoader = FakeImageLoader()
        val flickrPhoto = FlickrPhoto("id", "secret", "server", 0, "title");
        val list = arrayListOf(flickrPhoto)
        test = FlickrImageAdapter(list, imageLoader)
    }

    @Test
    fun count_should_be_2() {
        assertEquals(1, test.itemCount)
    }

    @Test
    fun count_should_be_zero() {
        test = FlickrImageAdapter(
            arrayListOf(),
            imageLoader
                                 )
        assertEquals(0, test.itemCount)
    }
}