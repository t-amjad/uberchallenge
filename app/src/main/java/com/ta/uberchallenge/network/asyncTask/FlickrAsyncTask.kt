package com.ta.uberchallenge.network.asyncTask

import android.content.Context
import android.os.AsyncTask
import com.ta.uberchallenge.home.model.Response
import com.ta.uberchallenge.network.api.FlickrApi
import com.ta.uberchallenge.network.parser.FlickrApiJSONParser

class FlickrAsyncTask constructor(
    private val mContext: Context,
    private val mCallback: AsyncTaskCallback,
    private val searchText: String,
    private val pageNo: Int
                                 ) :
    AsyncTask<Void, Response, Response>() {

    override fun doInBackground(vararg params: Void): Response? {
        val searchImagesApi = FlickrApi(mContext)
        val jsonObject = searchImagesApi.getImages(searchText, pageNo)
        val jsonParser = FlickrApiJSONParser()
        return jsonParser.parseSearchListResponse(jsonObject)
    }

    override fun onPostExecute(response: Response) {
        super.onPostExecute(response)
        mCallback.onTaskCompleted(response)
    }
}

interface AsyncTaskCallback {
    fun onTaskCompleted(response: Response)
}