package com.ta.uberchallenge.network.image

import android.graphics.Bitmap
import java.util.*

class MemoryCache {
    private val cache = Collections.synchronizedMap(
        LinkedHashMap<String, Bitmap>(10, 1.5f, true)
                                                   )
    private var size: Long = 0//current allocated size
    private var limit: Long = 1000000//max memory in bytes

    init {
        //use 25% of available heap size
        setLimit(Runtime.getRuntime().maxMemory() / 4)
    }

    private fun setLimit(new_limit: Long) {
        limit = new_limit
    }

    operator fun get(id: String): Bitmap? {
        return try {
            if (!cache.containsKey(id)) null else cache[id]
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
            null
        }

    }

    fun put(id: String, bitmap: Bitmap?) {
        try {
            if (cache.containsKey(id)) {
                size -= getSizeInBytes(cache[id])
            }
            cache[id] = bitmap
            size += getSizeInBytes(bitmap)
            checkSize()
        } catch (th: Throwable) {
            th.printStackTrace()
        }

    }

    private fun checkSize() {
        if (size > limit) {
            val iter = cache.entries.iterator()
            while (iter.hasNext()) {
                val entry = iter.next()
                size -= getSizeInBytes(entry.value)
                iter.remove()
                if (size <= limit)
                    break
            }
        }
    }

    fun clear() {
        cache.clear()
        size = 0
    }

    private fun getSizeInBytes(bitmap: Bitmap?): Long =
        if (bitmap == null) 0 else (bitmap.rowBytes * bitmap.height).toLong()
}
