package com.ta.uberchallenge.network.parser

import com.ta.uberchallenge.home.model.FlickrPhoto
import com.ta.uberchallenge.home.model.Response
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class FlickrApiJSONParser {

    fun parseSearchListResponse(jsonObject: JSONObject): Response? {
        var response: Response? = null
        if (jsonObject.has(KEY_ERROR)) {
            response = Response(error = jsonObject.getString(KEY_ERROR))
        } else if (jsonObject.optString(KEY_STAT) == KEY_OK) {
            val resultObject = jsonObject.getJSONObject(KEY_PHOTOS_JSON)
            val jsonArray = resultObject.getJSONArray(KEY_PHOTOS_ARRAY)
            response = Response(
                resultObject.optInt(KEY_PAGES, 0),
                parse(jsonArray)
                               )
        }
        return response
    }

    private fun parse(jsonArray: JSONArray): List<FlickrPhoto> {
        val flickrPhotoList = ArrayList<FlickrPhoto>()
        try {
            for (i in 0 until jsonArray.length()) {
                val jsonObj = jsonArray.getJSONObject(i)
                val flickrPhoto = FlickrPhoto(
                    jsonObj.optString(KEY_PHOTO_ID),
                    jsonObj.optString(KEY_SECRET),
                    jsonObj.optString(KEY_SERVER),
                    jsonObj.optInt(KEY_FARM),
                    jsonObj.getString(KEY_TITLE)
                                             )
                flickrPhotoList.add(flickrPhoto)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return flickrPhotoList
    }

    companion object {
        private const val KEY_ERROR = "error"
        private const val KEY_PHOTOS_JSON = "photos"
        private const val KEY_STAT = "stat"
        private const val KEY_OK = "ok"
        private const val KEY_PAGES = "pages"
        private const val KEY_PHOTOS_ARRAY = "photo"
        private const val KEY_PHOTO_ID = "id"
        private const val KEY_SECRET = "secret"
        private const val KEY_SERVER = "server"
        private const val KEY_FARM = "farm"
        private const val KEY_TITLE = "title"
    }
}
