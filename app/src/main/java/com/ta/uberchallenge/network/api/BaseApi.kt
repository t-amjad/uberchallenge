package com.ta.uberchallenge.network.api

import android.content.Context
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL

abstract class BaseApi constructor(protected var context: Context) {

    @Throws(java.lang.Exception::class)
    internal fun execute(
        requestMethod: RequestMethod,
        uri: String
                        ): Any {
        val url = URL(uri)

        System.setProperty("http.keepAlive", "false")
        val conn: HttpURLConnection = url.openConnection() as HttpURLConnection
        conn.connectTimeout = TIMEOUT
        conn.readTimeout = TIMEOUT
        conn.requestMethod = requestMethod.name
        conn.setRequestProperty("Charset", CHARSET_NAME)
        var inputStream: InputStream? = null
        try {
            inputStream = BufferedInputStream(conn.inputStream)
            val result = readStream(inputStream)
            conn.disconnect()
            inputStream.close()
            return JSONObject(result)
        } catch (e: Exception) {
            return JSONObject(
                "{\"error\":\"" + e.localizedMessage
                    .replace(":", "")
                    .replace("\"", "")
                        + "\"}"
                             )
        } finally {
            conn.disconnect()
            inputStream?.close()
        }
    }

    @Throws(Exception::class)
    private fun readStream(inputStream: InputStream): String {
        val builder = StringBuilder()
        var reader: BufferedReader? = null
        try {
            reader = BufferedReader(InputStreamReader(inputStream))
            var line = reader.readLine()
            while (line != null) {
                builder.append(line + "\n")
                line = reader.readLine()
            }
        } catch (e: java.lang.Exception) {
            throw e
        } finally {
            try {
                reader?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return builder.toString()
    }

    companion object {

        private const val TIMEOUT = 60000
        internal const val MAIN_BASE_URL = "https://api.flickr.com/"
        private const val CHARSET_NAME = "UTF-8"
        private const val API_KEY = "3e7cc266ae2b0e0d78e279ce8e361736"

        internal const val GET_SEARCH_IMAGES_URL =
            "services/rest/?method=flickr.photos.search&api_key=" +
                    API_KEY + "&format=json&nojsoncallback=1&safe_search=1"
    }

    enum class RequestMethod {
        GET
    }
}
