package com.ta.uberchallenge.network.image

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.annotation.DrawableRes
import android.widget.ImageView
import java.io.BufferedInputStream
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

open class ImageLoaderImpl : ImageLoader {

    private val memoryCache = MemoryCache()
    private val imageViewsMap = Collections.synchronizedMap(WeakHashMap<ImageView, String>())
    private val executorService: ExecutorService = Executors.newFixedThreadPool(5)

    override fun displayImage(@DrawableRes placeholder: Int, url: String, imageView: ImageView) {
        imageViewsMap[imageView] = url
        val bitmap = memoryCache.get(url)
        if (bitmap != null)
            imageView.setImageBitmap(bitmap)
        else {
            queuePhoto(url, imageView)
            imageView.setImageResource(placeholder)
        }
    }

    private fun queuePhoto(url: String, imageView: ImageView) {
        val p = PhotoToLoad(url, imageView)
        executorService.submit(PhotosLoader(p))
    }

    private fun getBitmap(url: String): Bitmap? {
        val bitmap: Bitmap?
        var conn: HttpURLConnection? = null
        var inputStream: InputStream? = null
        try {
            val imageUrl = URL(url)
            System.setProperty("http.keepAlive", "false")
            conn = imageUrl.openConnection() as HttpURLConnection
            conn.connectTimeout = 60000
            conn.readTimeout = 60000
            conn.requestMethod = "GET"
            conn.setRequestProperty("Charset", "UTF-8")

            try {
                inputStream = BufferedInputStream(conn.inputStream)
                bitmap = BitmapFactory.decodeStream(inputStream)
                conn.disconnect()
                inputStream.close()
            } catch (e: Exception) {
                return null
            } finally {
                conn.disconnect()
                inputStream?.close()
            }
            return bitmap
        } catch (ex: Throwable) {
            ex.printStackTrace()
            if (ex is OutOfMemoryError)
                memoryCache.clear()
            return null
        } finally {
            conn?.disconnect()
            inputStream?.close()
        }
    }

    inner class PhotoToLoad(var url: String, var imageView: ImageView)

    internal inner class PhotosLoader(private var photoToLoad: PhotoToLoad) : Runnable {

        override fun run() {
            if (imageViewReused(photoToLoad))
                return
            val bmp = getBitmap(photoToLoad.url)
            memoryCache.put(photoToLoad.url, bmp)
            if (imageViewReused(photoToLoad))
                return
            val bd = BitmapDisplayer(bmp, photoToLoad)
            val a = photoToLoad.imageView.context as Activity
            a.runOnUiThread(bd)
        }
    }

    private fun imageViewReused(photoToLoad: PhotoToLoad): Boolean {
        val tag = imageViewsMap[photoToLoad.imageView]
        return tag == null || tag != photoToLoad.url
    }

    internal inner class BitmapDisplayer(
        private var bitmap: Bitmap?,
        private var photoToLoad: PhotoToLoad
                                        ) :
        Runnable {
        override fun run() {
            if (imageViewReused(photoToLoad))
                return
            if (bitmap != null)
                photoToLoad.imageView.setImageBitmap(bitmap)
        }
    }
}

interface ImageLoader {
    fun displayImage(@DrawableRes placeholder: Int, url: String, imageView: ImageView)
}