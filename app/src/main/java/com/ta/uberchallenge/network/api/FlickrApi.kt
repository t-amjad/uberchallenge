package com.ta.uberchallenge.network.api

import android.content.Context
import org.json.JSONObject

class FlickrApi(context: Context) : BaseApi(context) {

    fun getImages(searchText: String, pageNo: Int): JSONObject {
        val result = execute(
            BaseApi.RequestMethod.GET,
            getImagesUrl(searchText, pageNo)
                            )
        if (result is JSONObject) {
            return result
        }
        return JSONObject("{\"error\":\"Something Went Wrong\"}")
    }

    private fun getImagesUrl(searchText: String, pageNo: Int): String {
        var url = BaseApi.MAIN_BASE_URL
        url += BaseApi.GET_SEARCH_IMAGES_URL
        url += SEARCH_TEXT_KEY + searchText
        url += SEARCH_PAGE_KEY + pageNo
        return url
    }

    companion object {
        private const val SEARCH_TEXT_KEY = "&text="
        private const val SEARCH_PAGE_KEY = "&page="
    }
}
