package com.ta.uberchallenge.utils

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog


class PermissionHandler constructor(
    private val mActivity: Activity,
    private val mPermissions: Array<String>,
    private val mRequestCode: Int,
    private val mListener: IPermissionsService
                                   ) {

    interface IPermissionsService {
        fun onPermissionGranted()
        fun onPermissionDenied()
    }

    fun hasPermissionsGranted(): Boolean {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            for (permission in mPermissions) {
                if (ContextCompat.checkSelfPermission(
                        mActivity,
                        permission
                                                     ) != PackageManager.PERMISSION_GRANTED
                ) {
                    return false
                }
            }
        }
        return true
    }

    fun requestPermission() {
        ActivityCompat.requestPermissions(mActivity, mPermissions, mRequestCode)
    }

    fun processResult(requestCode: Int, grantResults: IntArray) {
        if (requestCode == mRequestCode) {
            var result = 0
            for (item in grantResults) {
                result += item
            }
            if (result == PackageManager.PERMISSION_GRANTED) {
                mListener.onPermissionGranted()
            } else {
                mListener.onPermissionDenied()
            }
        }
    }

    fun showAlert(title: String, message: String) {
        AlertDialog.Builder(mActivity)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton("Ok") { _, _ -> gotoSettings() }
            .setNeutralButton("Close App") { _, _ -> mActivity.finish() }
            .create()
            .show()
    }

    private fun gotoSettings() {
        val intent = Intent().apply {
            action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            data = Uri.fromParts("package", mActivity.getPackageName(), null)
        }
        mActivity.startActivity(intent)
    }
}
