package com.ta.uberchallenge.home.model

class FlickrPhoto(
    var id: String,
    var secret: String,
    var server: String,
    var farm: Int,
    var title: String
                 ) {
    val imageURL: String
        get() {
            val imageUrl = "https://farm{farm}.static.flickr.com/{server}/{id}_{secret}.jpg"
                .replace("{farm}", farm.toString())
                .replace("{server}", server)
                .replace("{id}", id)
                .replace("{secret}", secret)
            return imageUrl
        }
}
