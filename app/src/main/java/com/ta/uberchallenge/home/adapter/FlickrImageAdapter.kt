package com.ta.uberchallenge.home.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.ta.uberchallenge.R
import com.ta.uberchallenge.home.model.FlickrPhoto
import com.ta.uberchallenge.network.image.ImageLoader

class FlickrImageAdapter(
    private val flickrPhotoList: ArrayList<FlickrPhoto>,
    private val imgLoader: ImageLoader
                        ) :
    RecyclerView.Adapter<FlickrImageAdapter.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val holder = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_photo, viewGroup, false)
        return ViewHolder(holder)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val flickrPhoto = flickrPhotoList[position]
        viewHolder.bindData(flickrPhoto)
    }

    override fun getItemCount(): Int {
        return flickrPhotoList.size
    }

    fun clear() {
        flickrPhotoList.clear()
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var mPhotoImageView: ImageView = itemView.findViewById(R.id.ivPhotoItem)

        lateinit var flickrPhoto: FlickrPhoto

        fun bindData(photo: FlickrPhoto) {
            flickrPhoto = photo
            mPhotoImageView.setImageDrawable(null)

            imgLoader.displayImage(
                R.mipmap.ic_launcher_round,
                flickrPhoto.imageURL,
                mPhotoImageView
                                  )
        }
    }
}