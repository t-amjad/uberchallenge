package com.ta.uberchallenge.home.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment

open class BaseFragment : Fragment() {

    private var context: Context? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        this.context = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (parentFragment == null) {
            retainInstance = true
            context
        }
    }

    override fun getContext(): Context? {
        return context
    }
}