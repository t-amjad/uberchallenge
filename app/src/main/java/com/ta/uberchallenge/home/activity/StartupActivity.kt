package com.ta.uberchallenge.home.activity

import android.Manifest
import android.annotation.TargetApi
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.ta.uberchallenge.utils.PermissionHandler

class StartupActivity : AppCompatActivity() {

    private lateinit var mPermissionHandler: PermissionHandler
    private val requestCode = 111

    @TargetApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPermissionHandler = PermissionHandler(
            this,
            arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
                   ),
            requestCode,
            object : PermissionHandler.IPermissionsService {
                override fun onPermissionGranted() {
                    handleNavigation()
                }

                override fun onPermissionDenied() {
                    mPermissionHandler.showAlert(
                        "Permission Required",
                        "This app needs Storage Permission to run"
                                                )
                }
            })
        checkStoragePermissions()
    }

    private fun checkStoragePermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !mPermissionHandler.hasPermissionsGranted()) {
            mPermissionHandler.requestPermission()
        } else {
            handleNavigation()
        }
    }

    private fun handleNavigation() {
        startActivity(Intent(this, HomeActivity::class.java))
        finish()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
                                           ) {
        mPermissionHandler.processResult(requestCode, grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}
