package com.ta.uberchallenge.home.fragment

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import com.ta.uberchallenge.R
import com.ta.uberchallenge.home.adapter.FlickrImageAdapter
import com.ta.uberchallenge.home.model.FlickrPhoto
import com.ta.uberchallenge.home.presenter.FlickrPresenter
import com.ta.uberchallenge.network.image.ImageLoaderImpl
import java.util.*

class HomeFragment : BaseFragment(), FlickrPresenter.View {

    private lateinit var inputEditText: EditText
    private lateinit var emptyView: TextView
    private lateinit var mProgressBar: ProgressBar

    private lateinit var flickrPresenter: FlickrPresenter
    private val photoList = ArrayList<FlickrPhoto>()
    private var mAdapter: FlickrImageAdapter? = null
    private var searchText = ""
    private var lastSearchText = ""
    private var pageNo = DEFAULT_PAGE
    private var mFetchingData: Boolean = false

    private val mTextChangeListener = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(query: CharSequence, start: Int, before: Int, count: Int) {
            searchText = inputEditText.text.toString()
        }

        override fun afterTextChanged(editable: Editable) {
            if (searchText.isEmpty()) {
                lastSearchText = ""
                mAdapter?.clear()
            } else if (searchText.length > 2) {
                if (!lastSearchText.contentEquals(searchText)) {
                    photoList.clear()
                    mAdapter?.notifyDataSetChanged()
                    pageNo = DEFAULT_PAGE
                    flickrPresenter.fetchImages(searchText, pageNo)
                }
                lastSearchText = searchText
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
                             ): View {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        flickrPresenter = FlickrPresenter(requireContext(), this)
        initComponents(view)
        return view
    }

    private fun initComponents(view: View) {
        val recyclerView = view.findViewById<RecyclerView>(R.id.rvSearchResults)
        inputEditText = view.findViewById(R.id.etSearch)
        emptyView = view.findViewById(R.id.emptyView)
        mProgressBar = view.findViewById(R.id.progressBar)
        mAdapter =
            FlickrImageAdapter(photoList, ImageLoaderImpl())
        recyclerView.adapter = mAdapter
        var pastVisibleItems = 0
        var visibleItemCount: Int
        var totalItemCount: Int
        val staggeredGridLayoutManager = StaggeredGridLayoutManager(
            GRID_SPAN_COUNT,
            StaggeredGridLayoutManager.VERTICAL
                                                                   )
        recyclerView.layoutManager = staggeredGridLayoutManager
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) {
                    visibleItemCount = staggeredGridLayoutManager.childCount
                    totalItemCount = staggeredGridLayoutManager.itemCount
                    var firstVisibleItems: IntArray? = null
                    firstVisibleItems =
                        staggeredGridLayoutManager.findFirstVisibleItemPositions(firstVisibleItems)
                    if (firstVisibleItems?.isNotEmpty() == true) {
                        pastVisibleItems = firstVisibleItems[0]
                    }

                    if (!mFetchingData) {
                        if (visibleItemCount + pastVisibleItems >= totalItemCount) {
                            flickrPresenter.fetchImages(searchText, pageNo++)
                        }
                    }
                }
            }
        })
        inputEditText.addTextChangedListener(mTextChangeListener)
    }

    override fun showError(error: String) {
        setEmptyView(true, error)
    }

    override fun showData(list: List<FlickrPhoto>) {
        photoList.addAll(list)
        mAdapter?.notifyDataSetChanged()
    }

    override fun showProgress() {
        mFetchingData = true
        mProgressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        mFetchingData = false
        mProgressBar.visibility = View.GONE
    }

    override fun setEmptyView(show: Boolean, txt: String) {
        emptyView.apply {
            visibility = if (show) View.VISIBLE else View.GONE
            text = txt
        }
    }

    companion object {
        private const val DEFAULT_PAGE = 1
        private const val GRID_SPAN_COUNT = 3
    }
}
