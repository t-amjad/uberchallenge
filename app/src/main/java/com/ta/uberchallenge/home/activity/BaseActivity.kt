package com.ta.uberchallenge.home.activity

import android.content.BroadcastReceiver
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import com.ta.uberchallenge.R
import com.ta.uberchallenge.receiver.NetworkStateChangeReceiver
import com.ta.uberchallenge.home.fragment.BaseFragment

abstract class BaseActivity : AppCompatActivity() {

    private var mNetworkReceiver: BroadcastReceiver? = null

    abstract fun getContainerId(): Int
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mNetworkReceiver = NetworkStateChangeReceiver()
        registerNetworkBroadcastForNougat()
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportFragmentManager.executePendingTransactions()
    }

    fun addFragment(
        baseFragment: BaseFragment
                   ) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(getContainerId(), baseFragment)
        fragmentTransaction.commitAllowingStateLoss()
    }


    private fun registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            this.registerReceiver(
                mNetworkReceiver,
                IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
                                 )
        }
    }

    private fun unregisterNetworkChanges() {
        try {
            unregisterReceiver(mNetworkReceiver)
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        }

    }

    public override fun onDestroy() {
        super.onDestroy()
        unregisterNetworkChanges()
    }
}
