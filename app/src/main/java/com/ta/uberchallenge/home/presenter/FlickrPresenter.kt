package com.ta.uberchallenge.home.presenter

import android.content.Context
import android.os.AsyncTask
import com.ta.uberchallenge.R
import com.ta.uberchallenge.home.model.FlickrPhoto
import com.ta.uberchallenge.home.model.Response
import com.ta.uberchallenge.network.asyncTask.AsyncTaskCallback
import com.ta.uberchallenge.network.asyncTask.FlickrAsyncTask

class FlickrPresenter(private val context: Context, private val view: View) :
    AsyncTaskCallback {

    fun fetchImages(searchText: String, pageNo: Int) {
        view.showProgress()
        FlickrAsyncTask(context, this, searchText, pageNo)
            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
    }

    override fun onTaskCompleted(response: Response) {
        view.hideProgress()
        when {
            response.error != null       -> view.showError(response.error)
            response.photoList.isEmpty() -> view.setEmptyView(
                true,
                context.getString(R.string.empty_list)
                                                             )
            else                         -> {
                view.setEmptyView(false)
                view.showData(response.photoList)
            }
        }
    }

    interface View {
        fun showData(list: List<FlickrPhoto>)
        fun showError(error: String)
        fun showProgress()
        fun hideProgress()
        fun setEmptyView(show: Boolean, txt: String = "")
    }
}