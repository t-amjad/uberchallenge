package com.ta.uberchallenge.home.model

class Response(
    val pages: Int = 0,
    val photoList: List<FlickrPhoto> = emptyList(),
    val error: String? = null
              )