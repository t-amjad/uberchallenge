package com.ta.uberchallenge.home.activity

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.LocalBroadcastManager
import com.ta.uberchallenge.R
import com.ta.uberchallenge.receiver.NetworkStateChangeReceiver
import com.ta.uberchallenge.receiver.NetworkStateChangeReceiver.Companion.IS_NETWORK_AVAILABLE
import com.ta.uberchallenge.home.fragment.HomeFragment

class HomeActivity : BaseActivity() {


    private var snackBar: Snackbar? = null

    override fun getContainerId() = R.id.fragmentContainer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.ta.uberchallenge.R.layout.activity_home)
        setUpNetworkConnectivityListener()
        if (savedInstanceState == null) {
            addFragment(HomeFragment())
        }
    }

    private fun setUpNetworkConnectivityListener() {
        val intentFilter = IntentFilter(NetworkStateChangeReceiver.NETWORK_AVAILABLE_ACTION)
        LocalBroadcastManager.getInstance(this).registerReceiver(object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val isNetworkAvailable = intent.getBooleanExtra(IS_NETWORK_AVAILABLE, false)
                if (!isNetworkAvailable) {
                    snackBar = Snackbar.make(
                        findViewById(R.id.coordinatorLayout),
                        R.string.error_no_internet, Snackbar.LENGTH_INDEFINITE
                                            )
                    snackBar?.show()
                } else {
                    snackBar?.dismiss()
                }
            }
        }, intentFilter)

    }
}
