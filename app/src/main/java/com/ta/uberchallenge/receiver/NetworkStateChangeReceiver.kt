package com.ta.uberchallenge.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.content.Intent
import android.net.ConnectivityManager
import android.support.v4.content.LocalBroadcastManager

class NetworkStateChangeReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        val networkStateIntent = Intent(NETWORK_AVAILABLE_ACTION)
        networkStateIntent.putExtra(IS_NETWORK_AVAILABLE, isConnectedToInternet(context))
        LocalBroadcastManager.getInstance(context!!).sendBroadcast(networkStateIntent)
    }


    private fun isConnectedToInternet(context: Context?): Boolean {
        try {
            val networkInfo =
                (context?.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo
            return networkInfo != null && networkInfo.isConnected
        } catch (e: Exception) {
            return false
        }

    }

    companion object {
        const val NETWORK_AVAILABLE_ACTION = "ta.uberchallenge.broadcast.NetworkAvailable"
        const val IS_NETWORK_AVAILABLE = "isNetworkAvailable"
    }
}