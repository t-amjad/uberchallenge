# UberChallenge

##How to run:

1. check out this repo and switch to master branch
2. Install IDE (Android Studio); with Android sdk version 27
3. load this Project into your IDE (Android studio)
4. run the app on devices/emulators with sdk version >= 15

## About this project:
This application mainly contains *BASIC* version of;

1. MVP Design Pattern
2. Usage of Local Broadcast receivers to handle network changes
3. Caching of images
4. Unit test for adapter and presenter
5. handling permissions
6. handles basic generic errors from API call
7. Used some interfaces to make code more testable

* `home`
	* activity
    	* `BaseActivity`
		* `StartupActivity`  *(to handle the startup like permisisons and stuff)*
    	* `HomeActivity` *(to host home fragment)*
	* adapter
    	* `FlickrImageAdapter`
	* fragment
    	* `BaseFragment`
	    * `HomeFragment` *(fetches and loads the adapter to displays list of images)*
	* models
    	* `FlickrPhoto` *(Model for Flickr Photo Object)*
	    * `Response` *(API response with list of FlickrPhotos or Error)*
	* presenter
    	* `FlickrPresenter` *(MVP design pattern- presenter class for hoem fragment)*
	* network
    	* api
        	* `BaseApi` *(making API Call)*
	        * `FlickrApi` *(to get flickr Images)*
    	* asyncTask
        	* `FlickrAsyncTask` *(to make API call on background thread)*
	    * image
    	    * `ImageLoaderImpl` *(for downloading and displaying Images)*
        	* `MemoryCache` *(Caches images in memroy after downloading)*
	    * parser
    	    * `FlickrApiJSONParser` *(Json parser and maps it into Objects/models)*
    * receiver
        * `NetworkStateChangeReceiver` *(Local Broadcast listener to emit networf connectivity changes)*
	* utils
    	* `PermissionHandler` *(handling Permissions)*
    
## Probable Improvements for future
* Improve Image Caching by saving files in phone memory with the combination os Memory Caching
* Improve Design Pattern
* More and More Unit Tests,
* unit test backed by instrumation Tests
* Material Design UI
* Add Proguard for code obfuscation.
* Using third party libraries to improve code quality and stability (Retrofil, RxJava, Gson, Dagger to name few)